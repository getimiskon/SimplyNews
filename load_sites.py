from sites import itsfoss
from sites import theverge
from sites import decentralizetoday
from sites import nypost
from sites import theguardian
from sites import heise
from sites import androidauthority

sites = {
    "androidauthority.com": androidauthority,
    "www.androidauthority.com": androidauthority,

    "heise.de": heise,
    "www.heise.de": heise,

    "theverge.com": theverge,
    "www.theverge.com": theverge,

    "itsfoss.com": itsfoss,

    "dt.gl": decentralizetoday,

    "nypost.com": nypost,
    "theguardian.com": theguardian,
}
